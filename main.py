# -*- coding: utf-8 -*-
"""
Created on Sun Mar 10 15:49:34 2024

@author: pc
"""
    
import random

def deviner_nombre():
    nombre_aleatoire = random.randint(1, 100)
    essais_restants = 10

    print("Bienvenue dans le jeu de devinette! Je pense à un nombre entre 1 et 100. Vous avez 10 essais pour deviner.")

    while essais_restants > 0:
        essai = int(input("Entrez votre devinette : "))
       
        if essai < nombre_aleatoire:
            print("Trop petit !")
        elif essai > nombre_aleatoire:
            print("Trop grand !") 
        else:
            print("Félicitation ! Vous avez deviné le nombre exact!")
          
        essais_restants -= 1
        print("Il vous reste {} essais.".format(essais_restants))

    print("Dommages! Le nombre était", nombre_aleatoire)
    return
deviner_nombre()
    
    
    

